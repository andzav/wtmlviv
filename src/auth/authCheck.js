module.exports = (req, res, next) => {
	if (req.session && req.session.userId) {
		return next();
	} else {
		req.session.info = 'You must be logged in to view this page';
		return res.redirect('/');
	}
};