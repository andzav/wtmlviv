let express = require('express');
let router = new express.Router();
let User = require('../models/user');
let validator = require("validator");

router.get('/', (req, res) => {
	let alert_text = req.session.info;
	req.session.info = "";
	res.render('login', {'info': alert_text});
});

router.post('/', (req, res, next) => {
	if(req.body.email && req.body.password && validator.isEmail(req.body.email)){
		User.authenticate(req.body.email, req.body.password, function (error, user) {
			if (error || !user) {
				req.session.info = 'Wrong email or password';
				return res.redirect('/auth');
			} else {
				req.session.userId = user._id;
				req.session.username = user.username;
				req.session.permission = user.permission;
				req.session.info = 'Successfully logged in';
				return res.redirect('/');
			}
		});
	} else {
		req.session.info = 'Please, fill all fields with correct information';
		return res.redirect('/auth');
	}
});

router.post('/register', (req, res, next) => {
	if (req.body.email && 
		validator.isEmail(req.body.email) &&
		req.body.username &&
		req.body.password &&
		req.body.passwordConf) {

		if (req.body.password !== req.body.passwordConf) {
			req.session.info = 'Passwords do not match';
			return res.redirect('/auth');
		} else {
			let userData = {
				email: req.body.email,
				username: req.body.username,
				password: req.body.password
			}

			User.create(userData, function (error, user) {
				if (error) {
					return next(error);
				} else {
					req.session.userId = user._id;
					req.session.username = user.username;
					req.session.permission = user.permission;
					req.session.info = 'Successfully logged in';
					return res.redirect('/');
				}
			});
		}
	} else {
		req.session.info = 'Please, fill all fields with correct information';
		return res.redirect('/auth');
	}
});

router.get('/logout', function(req, res, next) {
	if (req.session) {
		req.session.destroy(function(err) {
			if(err) {
				console.log(err)
				return res.redirect('/');
			} else {
				return res.redirect('/');
			}
		});
	}
});

module.exports = router;