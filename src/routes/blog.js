let express = require('express');
let router = new express.Router();
let BlogPost = require('../models/blogpost');
let md = require('markdown-it')({linkify: true, breaks: true, typographer:  true});
var emoji = require('markdown-it-emoji');
md.use(emoji);
let middleware = require('../auth/authCheck');
let validator = require("validator");

router.get('/', (req, res) => {
	BlogPost.find({}, '-body', {sort: "-created"}, function(err, data) {
		if(err){
			return next(err);
		} else {
			res.render('blog', {data: data, 'logged': req.session && req.session.permission, 'admin': req.session && req.session.permission==='admin'})
		}
	});
});

router.get('/post/:id', function(req, res, next) {
	BlogPost.findOne({_id: req.params.id}, '-body', function(err, data) {
		if(err || !data){
			return next(err);
		} else {
			res.render('blogpost', {data: data, 'logged': req.session && req.session.permission})
		}
	});
});

router.get('/new', middleware, function(req, res, next) {
	res.render('newpost', {'logged': req.session && req.session.permission});
});

router.post('/new', middleware, function(req, res, next) {
	if(req.body.caption && req.body.body){
		
		let submissionData = {
			caption: req.body.caption,
			body: req.body.body,
			preview: md.render(req.body.body.substring(0, 120)).replace(/(\r\n\t|\n|\r\t)/gm,""),
			creator: req.session.username,
			rendered: "<img src='"+req.body.imgurl+"'/>"+md.render(req.body.body).replace(/(\r\n\t|\n|\r\t)/gm,""),
			created: Date.now()
		}

		BlogPost.create(submissionData, function (error) {
			if (error) {
				console.log(error)
				return res.redirect('/newpost')
			} else {
				return res.render('newpost', {alert: "Success"})
			}
		});
		
	} else {
		return res.render('newpost', {alert: "All fields required"})
	}
});

module.exports = router;