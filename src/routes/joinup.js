let express = require('express');
let router = new express.Router();
let Submission = require('../models/submission');
let middleware = require('../auth/authCheck');
let validator = require("validator");

router.get('/', (req, res) => {
	Submission.find({}, '-_id', function(err, data) {
		if(err){
			return next(err);
		} else {
			let alert_text = req.session.info;
			req.session.info = "";
			if (req.session && req.session.permission=='admin')
				res.render('joinus', {data: data, 'logged': req.session && req.session.permission, 'info': alert_text})
			else
				res.render('joinus', {data: [], 'logged': req.session && req.session.permission, 'info': alert_text})
		}
	});
});

router.get('/list', function(req, res, next) {
	Submission.find({}, '-_id', function(err, data) {
		if(err){
			res.sendStatus(400);
		} else {
			res.json(data);
		}
	})
});

router.post('/', (req, res, next) => {
	if(req.body.email && req.body.name && req.body.comment && validator.isEmail(req.body.email)){
		let submissionData = {
			email: req.body.email,
			name: req.body.name,
			comment: req.body.comment
		}

		Submission.create(submissionData, function (error, user) {
			if (error) {
				return res.redirect('/join')
			} else {
				req.session.info = "Success"
				return res.redirect('/join')
			}
		});
		
	} else {
		req.session.info = "All fields required"
		return res.redirect('/join')
	}
});

module.exports = router;