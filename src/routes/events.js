let express = require('express');
let router = new express.Router();
let Event = require('../models/event');
let md = require('markdown-it')({linkify: true, breaks: true, typographer:  true});
var emoji = require('markdown-it-emoji');
md.use(emoji);
let middleware = require('../auth/authCheck');
let validator = require("validator");

router.get('/', (req, res) => {
	Event.find({}, '', {sort: '-scheduled'}, function(err, data) {
		if(err){
			return next(err);
		} else {
			let alert_text = req.session.info;
			req.session.info = "";
			res.render('events', {data: data, 'info': alert_text, 'logged': req.session && req.session.permission, 'admin': req.session && req.session.permission==='admin'})
		}
	});
});

router.get('/e/:id', function(req, res, next) {
	Event.findOne({_id: req.params.id}, function(err, data) {
		if(err || !data){
			return next(err);
		} else {
			let alert_text = req.session.info;
			req.session.info = "";
			res.render('event', {data: data, 'info': alert_text})
		}
	});
});

router.get('/new', middleware, function(req, res, next) {
	let alert_text = req.session.info;
	req.session.info = "";
	if(req.session.permission==="admin") res.render('newevent', {'info': alert_text});
	else res.redirect('/events')
});

router.post('/new', middleware, function(req, res, next) {
	if(req.body.caption && req.body.preview && req.body.location && req.body.info && req.body.scheduled && req.body.price && req.session.permission==="admin"){
		let submissionData = {
			caption: req.body.caption,
			preview: req.body.preview,
			location: req.body.location || req.body.location2,
			info: req.body.info.substring(0, 128).replace(/(\r\n\t|\n|\r\t)/gm,""),
			rendered: md.render(req.body.info).replace(/(\r\n\t|\n|\r\t)/gm,""),
			scheduled: new Date(req.body.scheduled),
			price: req.body.price
		}

		Event.create(submissionData, function (error) {
			if (error) {
				return res.redirect('/events/new');
			} else {
				req.session.info = "Success";
				return res.redirect('/events');
			}
		});
		
	} else {
		req.session.info = "All fields required"
		return res.redirect('/events/new')
	}
});

module.exports = router;