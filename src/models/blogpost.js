let mongoose = require('mongoose');
let shortID = require('shortid');

let BlogpostSchema = new mongoose.Schema({
	_id: {type: String, 'default': shortID.generate},
	caption: {
		type: String,
		required: true,
		trim: true
	},
	preview: {
		type: String,
		required: true,
		trim: true
	},
	body: {
		type: String,
		required: true,
		trim: true
	},
	creator: {
		type: String,
		required: true
	},
	rendered: {
		type: String,
		required: true
	},
	created: {
		type: Date,
		required: true
	}
});

let Blogpost = mongoose.model('Blogpost', BlogpostSchema);

module.exports = Blogpost;