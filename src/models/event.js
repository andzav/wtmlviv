let mongoose = require('mongoose');
let shortID = require('shortid');

let EventSchema = new mongoose.Schema({
	_id: {type: String, 'default': shortID.generate},
	caption: {
		type: String,
		required: true,
		trim: true
	},
	preview: {
		type: String,
		required: true,
		trim: true
	},
	location: {
		type: String,
		required: true
	},
	rendered: {
		type: String,
		required: true
	},
	info: {
		type: String,
		required: true
	},
	scheduled: {
		type: Date,
		required: true
	},
	price: {
		type: Number,
		default: 0.0
	}
});

let Event = mongoose.model('Event', EventSchema);

module.exports = Event;