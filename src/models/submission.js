let mongoose = require('mongoose');

let SubmissionSchema = new mongoose.Schema({
	email: {
		type: String,
		unique: true,
		required: true,
		trim: true
	},
	name: {
		type: String,
		unique: true,
		required: true,
		trim: true
	},
	comment: {
		type: String,
		required: true,
		trim: true
	}
});

let Submission = mongoose.model('Submission', SubmissionSchema);

module.exports = Submission;