let express = require('express');
let compression = require('compression');
let morgan = require('morgan');
let session = require('express-session');
let MongoStore = require('connect-mongo')(session);
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let md = require('markdown-it')();
let pug = require('pug');

let app = express();

mongoose.Promise = global.Promise;
let options = {
	reconnectTries: Number.MAX_VALUE,
	useNewUrlParser: true,
	reconnectInterval: 100,
	poolSize: 10,
	bufferMaxEntries: 0
};
mongoose.connect("-", options);

let sess = {
	secret: 'wtmLvIv',
	resave: true,
	store: new MongoStore({ mongooseConnection: mongoose.connection }),
	saveUninitialized: false
}

app.set('trust proxy', 1);
app.use(session(sess));
app.use(compression())
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.set('view engine', 'pug');
app.use(morgan('dev'));
app.use(express.static(__dirname + '/resources'));

app.get('/.well-known/acme-challenge/PfGHUGXPBSdRp46ut_uPXbhp8FER9EyDvNJ9A_uVPpo', (req, res) => {
	res.set('Content-Type', 'text/plain');
	res.send("PfGHUGXPBSdRp46ut_uPXbhp8FER9EyDvNJ9A_uVPpo.8mC2guXJdGA8DUYskbq3l0J8Vk4k4NDnHyPQ1edwA5s");
});
app.get('/.well-known/acme-challenge/HAfARSYe5OXW-sPkw08ZqKtcQwbeSJMKTe1I_Ri_b14', (req, res) => {
	res.set('Content-Type', 'text/plain');
	res.send("HAfARSYe5OXW-sPkw08ZqKtcQwbeSJMKTe1I_Ri_b14.8mC2guXJdGA8DUYskbq3l0J8Vk4k4NDnHyPQ1edwA5s");
});

let http = require("http");
setInterval(function() {
	http.get("http://wtmlviv.herokuapp.com/");
}, 300000); // every 5 minutes (300000)

var loginRoute = require('./src/auth/login');
let middleware = require('./src/auth/authCheck');
app.use('/auth', loginRoute);
app.get('/admin', middleware, (req, res) => res.json({"logged": true}))

let joinUpRouter = require('./src/routes/joinup');
let blogRouter = require('./src/routes/blog');
let eventsRouter = require('./src/routes/events');
app.use('/join', joinUpRouter);
app.use('/blog', blogRouter);
app.use('/events', eventsRouter);

app.get('/', (req, res) => {
	let alert_text = req.session.info;
	req.session.info = "";
	res.render('main', {'main': true, 'logged': req.session && req.session.permission, info: alert_text})
});
app.get('/contacts', (req, res) => {
	let alert_text = req.session.info;
	req.session.info = "";
	res.render('contact', {'logged': req.session && req.session.permission, info: alert_text})
});
app.get('/manifest.appcache', (req, res) => res.sendFile(__dirname + '/views/manifest.appcache'));

app.listen(process.env.PORT || 8080);